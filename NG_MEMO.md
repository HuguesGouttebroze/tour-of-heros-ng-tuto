# NG MEMO

```jsx
// data binding
<div>{{ propertyName }}</div>

// binding from an object type, here `hero` that get an `id` and a `name` as attributes
<h2>{{hero.name}} details</h2>
<div><span>id: </span>{{hero.id}}</div>
<div><span>name: </span>{{hero.name}}</div>

// Format with the UppercasePipe as
<h2>{{hero.name | uppercase}} Details</h2>

// Two-way binding as
<div>
  <label for="name">Hero name: </label>
  <input id="name" [(ngModel)]="hero.name" placeholder="name">
</div>

{/* + into app module*/}
import { FormsModule } from '@angular/forms';
imports: [
  BrowserModule,
  FormsModule
],

{/* NgFor directive */}


```

## Component VS Service

### Components

- Les **composants** :

  - ne doivent pas récupérer ou enregistrer des données directement.

  - ne doivent pas sciemment présenter de fausses données.

  - doivent se concentrer sur la _présentation des données_ et _déléguer_ l’_accès aux données_ à un **service**.

### Services

- Au lieu de créer un **service** avec le mot-clé `new`, on utilise l' **injection de dépendances** prise en charge par **Angular** pour l'injecter dans le constructeur des **components**.

- Toutes les classes d'application on accès aux donnés du service.

- Les **services** sont un excellent moyen de _partager des informations entre classes qui ne se connaissent pas_.

## Router

- Générons le fichier `src/app/app-routing.module.ts` avec la **CLI**

```sh
ng generate module app-routing --flat --module=app
```

| PARAMÈTRE      |                                                                  DÉTAILS |
| :------------- | -----------------------------------------------------------------------: |
| `--flat`       |    Place le fichier dans `src/app`, à la place de son propre répertoire. |
| `--module=app` | Indique ng generatede l'enregistrer dans le importstableau du AppModule. |

- Voici le fichier `app-routing.module.ts`, qui importe `RouterModule` et `Routes`

```ts
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HeroesComponent } from "./heroes/heroes.component";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "heroes",
    component: HeroesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
```

### Itinéraires

- Les routes indiquent au routeur quelle vue afficher lorsqu'un utilisateur clique sur un lien ou colle une URL dans la barre d'adresse du navigateur.

- Voici la partie du fichier où nous configurons nos itinéraires:

```ts
const routes: Routes = [
  {
    path: "heroes",
    component: HeroesComponent,
  },
];
```

| PROPRIÉTÉS  | DÉTAILS                                                                           |
| :---------: | --------------------------------------------------------------------------------- |
|   `path`    | Une chaîne qui correspond à l'URL dans la barre d'adresse du navigateur.          |
| `component` | Le composant que le routeur doit créer lors de la navigation vers cet itinéraire. |

- Ce qui indique au routeur de faire correspondre le **path** (**URL**) : `heroes`et d'afficher le `HeroesComponent` lorsque l'**URL** ressemble à `localhost:4200/heroes`.

### RouterModule.forRoot()

- Les métadonnées initialisent le routeur et le démarrent en écoutant les changements d'emplacement du navigateur.

- La ligne suivante ajoute le **RouterModule** au tableau **AppRoutingModule** et le configure avec `routes` en une seule étape en appelant `RouterModule.forRoot()` :

```ts
imports: [RouterModule.forRoot(routes)];
```

- Rend `RouterModule` disponible dans toute l’application.

```ts
exports: [RouterModule];
```

### RouterOutlet

- Ds AppComponent, ns remplaçons `<app-heroes>` par `<router-outlet>`

```html
<h1>{{title}}</h1>
<router-outlet></router-outlet>
<app-messages></app-messages>
```

- Le modèle `<app-heroes>` n'est plus nécessaire car l'application ne l'affiche que lorsque l'utilisateur accède à `HeroesComponent`.

- `<router-outlet>` indique au routeur où afficher les vues routées.

### Ajoutez un lien de navigation en utilisant routerLink

- Ajoutons un élément `<nav>` et, à l'intérieur, un élément d'ancrage qui, lorsque nous cliquons dessus, déclenche la navigation vers le fichier `HeroesComponent` :

```html
<h1>{{title}}</h1>
<nav>
  <a routerLink="/heroes">Heroes</a>
</nav>
<router-outlet></router-outlet>
<app-messages></app-messages>
```
