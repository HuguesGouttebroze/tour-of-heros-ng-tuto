import { NgModule } from '@angular/core';
import { HeroesComponent } from './heroes/heroes.component';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';

const routes: Routes = [
  {
    // itinéraire par défaut
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
    // redirige une URL correspondante entièrement au chemin vide, vers la route dont le chemin est '/dashboard'
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
  },
  // itinéraire paramétré (idem aux autres outils de routage `js`, type `react router`)
  {
    path: 'detail/:id', //Le caractère `:` indique un espace réservé pour un héros spécifique, son `id`
    component: HeroDetailComponent,
  },
  {
    path: 'heroes',
    component: HeroesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
